import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  language: string;
  constructor(private translate: TranslateService) {

   }

  ngOnInit(): void {
  }

  selectChage(event:any){
    this.language=event.target.value;
    this.translate.use(this.language);
  }
}
