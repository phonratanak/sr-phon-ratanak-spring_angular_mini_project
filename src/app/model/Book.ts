import { Category } from './Category';
export class Book {
  title:String;
  author:String;
  description:String;
  thumbnail:String;
  categories:Category;
}
