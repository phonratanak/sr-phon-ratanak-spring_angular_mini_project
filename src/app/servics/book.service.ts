import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrlBook= 'http://localhost:8080/books/';
const baseUrlImage= 'http://localhost:8080/upload';
@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any>{
    return this.http.get<any>(baseUrlBook);
  }
  onDelete(url:any): Observable<any>{
    return this.http.delete<any>(url);
  }

  postImage(data:any): Observable<any>{
    return this.http.post<any>(baseUrlImage,data);
  }

  addBook(book:any): Observable<any>{
    return this.http.post<any>(baseUrlBook,book)
  }

  getBookById(id:any): Observable<any>{
    return this.http.get<any>(baseUrlBook+id);
  }

  putBook(url:string,book:any): Observable<any>{
    console.log(book);

    return this.http.put<any>(url,book);
  }
}
