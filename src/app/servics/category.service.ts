import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const categoryUrl="http://localhost:8080/categories/";

@Injectable({
  providedIn: 'root'
})

export class CategoryService {
  constructor(private http:HttpClient) { }

  getCategory(): Observable<any>{
    return this.http.get<any>(categoryUrl);
  }

  onDelete(url:any): Observable<any>{
    return this.http.delete<any>(url);
  }

  addCategory(category:any): Observable<any>{
    return this.http.post<any>(categoryUrl,category);
  }

  putCategory(url:string,title:any): Observable<any>{
    return this.http.put<any>(url,title);
  }

  getCategoryById(id:any): Observable<any>{
    return this.http.get<any>(categoryUrl+id);
  }
}
