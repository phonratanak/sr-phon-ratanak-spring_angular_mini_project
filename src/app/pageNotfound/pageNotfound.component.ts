import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pageNotfound',
  templateUrl: './pageNotfound.component.html',
  styleUrls: ['./pageNotfound.component.css']
})
export class PageNotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
