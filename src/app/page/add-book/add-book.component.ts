import { Component, OnInit} from '@angular/core';
import { BookService } from '../../servics/book.service';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../servics/category.service';
import { Book } from '../../model/Book';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NotificationService } from '../../notification.service';
import { NotificationType } from '../../notification.message';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {
  //if user not selection image it get default
  urlImage:any='';
  submited=false;
  datas:any;
  category:any;
  formGroup:FormGroup;
  fileDatas: File = null;
  bookData:Book;
  url='/page-home';
  constructor(private notificationService: NotificationService,private bookServic: BookService,private bl:FormBuilder,private categoryService:CategoryService,private router:Router) { }

  ngOnInit():void {
    this.formGroup=this.bl.group({
      title:['',[Validators.required]],
      author:['',[Validators.required]],
      categories:this.bl.group(
        {
          id:['',[Validators.required]]
        }
      ),
      description:[''],
      thumbnail:[''],
    });
    this.getCategory();
  }

  getCategory(){
      this.categoryService.getCategory().subscribe(
        data =>{
          this.category=data._embedded.categories;
        },
        error =>{
          console.log(error);
        }
      )
  }

  get f(){return this.formGroup.controls;}

  onSubmit(){
    this.submited= true;
    this.datas=this.formGroup.value;
    if(this.formGroup.invalid)
    {return;}

    if(this.fileDatas == null)
    {
      this.addBook(this.datas);
    }else
    {
      this.submitIamge();
    }
  }

  addBook(data:any){
    this.bookServic.addBook(this.datas).subscribe(
      data =>{
        this.msgSuccess("Add Book is successfully.");
        this.getToHomepage(this.url);
      },
      error =>{
        this.msgWaring("Add Book is not successfully.");
        this.getToHomepage(this.url);
      }
    )
  }

  selectImage(event:any){
    this.fileDatas = <File>event.target.files[0];
  }

  submitIamge(){
    const formData = new FormData();
    formData.append('file', this.fileDatas);
    this.bookServic.postImage(formData).subscribe(
      data =>{
        this.urlImage=data.url;
        this.datas.thumbnail=this.urlImage;
        this.addBook(this.datas);
      },
      error =>{
      console.log(error);
    })
  }

  getToHomepage(url:String){
    this.router.navigate([url]);
  }

  msgSuccess(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.success
    });
  }
  msgWaring(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.error
    });
  }
}
