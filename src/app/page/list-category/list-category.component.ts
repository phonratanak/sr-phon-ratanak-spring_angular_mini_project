import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../servics/category.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../notification.service';
import { NotificationType } from '../../notification.message';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})
export class ListCategoryComponent implements OnInit {
  categoryForm:FormGroup;
  categoryData:any;
  submited=false;
  btnName:string;
  urlUpdate:any;
  p: number = 1;
  term:string;
  constructor(private notificationService: NotificationService,private category: CategoryService,private builder:FormBuilder) { }

  ngOnInit() {
    this.btnName="ADD"
    this.categoryForm=this.builder.group({
      title:['',Validators.required]
    })
    this.getCategory();
  }

  get f(){return this.categoryForm.controls;}

  addCategory(){
    this.submited= true;
    if(this.categoryForm.invalid)
    {return;}
    if(this.btnName=="ADD"){
      this.category.addCategory(this.categoryForm.value).subscribe(
        data =>{
          this.msgSuccess("Add Category success.")
          this.getCategory();
        },
        error =>{
          this.msgWaring("add Category not success.")
        }
      )
    }else{
      this.category.putCategory(this.urlUpdate,this.categoryForm.value).subscribe(
        data =>{
          this.msgSuccess("Upadate Category success.")
          this.getCategory();
        },
        error =>{
          this.msgWaring("Upadate Category not success.")
        }
      )
    }

  }

  getCategory(){
    this.category.getCategory().subscribe(
      data =>{
        this.categoryData=data._embedded.categories;
      },
      error =>{
        console.log(error);
      }
    )
  }

  deleteCategory(url:string){
    this.category.onDelete(url).subscribe(
      data =>{
        this.msgSuccess("Delete Category is success.")
        this.getCategory();
      },
      error =>{
        this.msgWaring("Delete Category not success.")
      }
    )
  }

  updateCategory(url:string,id:any){
    this.category.getCategoryById(id).subscribe(
      data=>{
        this.btnName="EDIT"
        this.categoryForm.controls["title"].setValue(data.title);
        this.urlUpdate=url;
      },
      error =>{

      }
    )
  }
  msgSuccess(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.success
    });
  }
  msgWaring(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.error
    });
  }

}
