import { Component, OnInit } from '@angular/core';
import { BookService } from '../../servics/book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../../model/Book';

@Component({
  selector: 'app-viewbook',
  templateUrl: './viewbook.component.html',
  styleUrls: ['./viewbook.component.css']
})
export class ViewbookComponent implements OnInit {
  book:Book;
  constructor(private route: ActivatedRoute ,private bookService: BookService,private router:Router) { }

  ngOnInit(): void {
    this.getBookById(this.route.snapshot.params['id']);
  }

  getBookById(id:any)
  {
    this.bookService.getBookById(id).subscribe(
      data =>{
        this.book=data;
        console.log(this.book);
      },
      error =>{
        console.log(error);
      }
    );
  }
  backHome()
  {
    this.router.navigate(["/page-home"]);
  }

}
