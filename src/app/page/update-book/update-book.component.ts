import { Component, OnInit } from '@angular/core';
import { BookService } from '../../servics/book.service';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../servics/category.service';
import { Book } from '../../model/Book';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Category } from '../../model/Category';
import { NotificationService } from '../../notification.service';
import { NotificationType } from '../../notification.message';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {
  urlUpdate:any;
  urlImage:any='';
  submited=false;
  datas:any;
  categorys:any;
  formGroup:FormGroup;
  fileDatas: File = null;
  bookData:Book;
  category:Category;
  constructor(private notificationService: NotificationService,private bookServic: BookService,private bl:FormBuilder,private categoryService:CategoryService,private router:Router,private route: ActivatedRoute) { }

  ngOnInit():void {
    this.getBookById(this.route.snapshot.params['id']);
    this.formGroup=this.bl.group({
      title:['fff',[Validators.required]],
      author:['',[Validators.required]],
      categories:this.bl.group(
        {
          id:['',[Validators.required]]
        }
      ),
      description:[''],
      thumbnail:[''],
    });
    this.getCategory();
  }

  getBookById(id:any)
  {
    this.bookServic.getBookById(id).subscribe(
      data =>{
        this.urlUpdate=data._links.book.href;

        this.formGroup.controls["title"].setValue(data.title);
        this.formGroup.controls["author"].setValue(data.author);
        this.formGroup.controls["description"].setValue(data.description);
        this.formGroup.controls["thumbnail"].setValue(data.thumbnail);
        this.formGroup.get("categories.id").patchValue(data.categories.resourceId);
      },
      error =>{
        console.log(error);
      }
    );
  }

  getCategory(){
      this.categoryService.getCategory().subscribe(
        data =>{
          this.categorys=data._embedded.categories;
        },
        error =>{
          console.log(error);
        }
      )
  }

  get f(){return this.formGroup.controls;}

  onSubmit(){
    let url='/page-home';
    this.submited= true;
    this.datas=this.formGroup.value;

    if(this.formGroup.invalid)
    {return;}

    if(this.fileDatas == null)
    {
      this.updateBook(this.urlUpdate,this.datas);
    }else
    {
      this.submitIamge();
    }
    this.getToHomepage(url);
  }

  updateBook(url:string,data:any){
    this.bookServic.putBook(url,data).subscribe(
      data =>{
        this.msgSuccess("Update is successfully.")
      },
      error =>{
       this.msgWaring("Update is not success.")
      }
    )
  }

  selectImage(event:any){
    this.fileDatas = <File>event.target.files[0];
  }

  submitIamge(){
    const formData = new FormData();
    formData.append('file', this.fileDatas);
    this.bookServic.postImage(formData).subscribe(
      data =>{
        this.urlImage=data.url;
        this.datas.thumbnail=this.urlImage;
        this.updateBook(this.urlUpdate,this.datas);
      },
      error =>{
      console.log(error);
    })
  }

  getToHomepage(url:String){
    this.router.navigate([url]);
  }

  msgSuccess(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.success
    });
  }
  msgWaring(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.error
    });
  }
}
