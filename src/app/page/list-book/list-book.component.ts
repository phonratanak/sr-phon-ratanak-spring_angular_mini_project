import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/servics/book.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../notification.service';
import { NotificationType } from '../../notification.message';
@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.scss']
})
export class ListBookComponent implements OnInit {
  data:any;
  datas:any;
  p: number = 1;
  term:string;
  constructor(private notificationService: NotificationService, private bookServic: BookService,private router:Router) { }

  ngOnInit() {
    console.log();
    this.getAllBooks();
  }

  getAllBooks(){
    this.bookServic.getAll()
    .subscribe(
      data1 => {
        this.datas=data1._embedded.books;
      },
      error => {
        console.log(error);
      });
  }

  onDelete(url:any){
    this.bookServic.onDelete(url).subscribe(
      data =>{
        this.msgSuccess("Delete is successfully.")
        this.getAllBooks();
      },
      error =>{
        this.msgWaring("Delete is not successfully.")
      }
    )
  }

  msgSuccess(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.success
    });
  }
  msgWaring(message:any) {
    this.notificationService.sendMessage({
      message: message,
      type: NotificationType.error
    });
  }

}
