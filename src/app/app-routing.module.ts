import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBookComponent } from './page/list-book/list-book.component';
import { ListCategoryComponent } from './page/list-category/list-category.component';
import { PageNotfoundComponent } from './pageNotfound/pageNotfound.component';
import { AddBookComponent } from './page/add-book/add-book.component';
import { CommonModule } from '@angular/common';
import { UpdateBookComponent } from './page/update-book/update-book.component';
import { ViewbookComponent } from './page/viewbook/viewbook.component';

const routes: Routes = [
  {path: '', redirectTo:'page-home',pathMatch: 'full'},
  {path: 'page-home',component: ListBookComponent},
  {path: 'add-new-book',component: AddBookComponent},
  {path: 'update-new-book/:id',component: UpdateBookComponent},
  {path: 'view-book/:id',component: ViewbookComponent},
  {path: 'page-category',component: ListCategoryComponent},
  {path: '**',component: PageNotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),CommonModule],
  exports: [RouterModule,CommonModule]
})
export class AppRoutingModule { }
