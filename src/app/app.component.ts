import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';
import defaultLanguage from "./../assets/i18n/en.json";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'maniProject';
  constructor(private translate: TranslateService) {
    translate.setTranslation('en', defaultLanguage);
    translate.setDefaultLang('en');
  }
}
